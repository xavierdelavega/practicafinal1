package view;

import model.Tablero;

public class TableroView {

    private Tablero tablero;

    public TableroView(Tablero tablero) {
        this.tablero = tablero;
    }

    public void dibujarTablero() {
        System.out.println("\n " + tablero.getPosicion(1) + " | " + tablero.getPosicion(2) + " | " + tablero.getPosicion(3));
        System.out.println("-----------");
        System.out.println(" " + tablero.getPosicion(4) + " | " + tablero.getPosicion(5) + " | " + tablero.getPosicion(6));
        System.out.println("-----------");
        System.out.println(" " + tablero.getPosicion(7) + " | " + tablero.getPosicion(8) + " | " + tablero.getPosicion(9) + "\n");
    }
}
