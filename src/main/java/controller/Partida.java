package controller;

import model.Ficha;
import model.Puntuacion;
import model.Tablero;
import model.Turno;
import view.TableroView;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Partida {

    private Puntuacion puntuacion;

    public Partida(Puntuacion puntuacion) {
        this.puntuacion = puntuacion;

        Scanner scanner = new Scanner(System.in);
        int posicion;

        Tablero tablero = new Tablero();
        TableroView tableroView = new TableroView(tablero);
        Turno turno = new Turno(Ficha.X);

        tableroView.dibujarTablero();

        while (!tablero.hayGanador() && !tablero.estaLleno()){

            do{
                System.out.println("Juega " + turno + ", entra posicion:");
                try {
                    posicion = scanner.nextInt();
                    scanner.nextLine();
                } catch (InputMismatchException IME) {
                    posicion = 0;
                    scanner.nextLine();
                }
            }while (posicion<1 || posicion>9);

            if (tablero.posicionLibre(posicion)){
                tablero.ponerFicha(posicion, turno.getFicha());
                turno.cambiarTurno();
                tableroView.dibujarTablero();
            } else {
                System.out.println("Posición ocupada, escoge otra.");
            }
        }

        if (tablero.hayGanador()) {
            turno.cambiarTurno();
            if (turno.getFicha().equals(Ficha.X)) puntuacion.ganaX();
            else puntuacion.ganaO();
            System.out.println("Gana " + turno + "!\n");
        }
        else if(tablero.estaLleno()) System.out.println("Empate!\n");
    }
}